#include "../headers/Stream.h"
using namespace StreamApp;

class Printer {

	std::ostream& os;

public:
	Printer(std::ostream& os) :
			os { os } {
	}

	template<typename T>
	void operator()(const T& t) {
		os << t << '\n';
	}

};

const Printer printer(std::cout);

int main() {



	Stream<char>::of(std::cin);











	return 0;
}
