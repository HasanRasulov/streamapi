#ifndef STREAM_H_
#define STREAM_H_

#include<functional>
#include<initializer_list>
#include<limits>
#include<vector>
#include<deque>
#include<stdexcept>
#include<array>
#include<list>
#include<forward_list>
#include<set>
#include<algorithm>
#include<iostream>
#include<memory>
#include<optional>
#include<utility>
#include<iterator>
#include<fstream>
#include<type_traits>
#include<string>

namespace StreamApp{

template<typename T>
class Stream {

public:

	enum class Style
		:unsigned char {
			lines, words, chars, tokens
	};

	class Builder {

		Stream<T>* stream;

	public:

		Builder(Stream<T>* stream) {
			this->stream = stream;
		}

		Builder& add(T item) {

			stream->data.push_back(item);

			return *this;
		}
		Builder& add(std::initializer_list<T> items) {

			stream->data.insert(stream->data.begin(), items);

			return *this;
		}

		Stream<T>& build() const {
			return stream;
		}
		~Builder() = default;
	};

protected:

	static std::size_t limit;
	std::vector<T> data;

	typename Stream<T>::Builder* builder_;

	Stream(std::initializer_list<T> items);
	Stream(std::size_t);
	Stream(std::vector<T>&& items);
	Stream(const Stream<T>&) = default;
	Stream() :
			builder_ { nullptr } {
	}

public:

	using Predicate = std::function<bool(const T&)>;

	using Comparator = std::function<bool(const T&,const T&)>;

	using Consumer = std::function<void(const T&)>;

	using BinaryOperation = std::function<T(const T&,const T&)>;

	using Builder_t = typename Stream<T>::Builder;

	using Style = typename Stream<T>::Style;

	Builder_t& builder();

	static std::unique_ptr<Stream<T>> concat(const Stream<T>& first,
			const Stream<T>& second);

	static std::unique_ptr<Stream<T>> empty();

	static std::unique_ptr<Stream<T>> generate(std::function<T()>)
			noexcept(false);

	static std::unique_ptr<Stream<T>> iterate(T seed, std::function<T(T)>)
			noexcept(false);

	static std::unique_ptr<Stream<T>> of(std::initializer_list<T>);

	static std::unique_ptr<Stream<T>> of(T);

	static std::unique_ptr<Stream<T>> of(const std::vector<T>&);

	static std::unique_ptr<Stream<T>> of(const std::deque<T>&);

	static std::unique_ptr<Stream<T>> of(const std::list<T>&);

	static std::unique_ptr<Stream<T>> of(const std::forward_list<T>&);

	static std::unique_ptr<Stream<T>> of(const std::set<T>&);

	static std::unique_ptr<Stream<T>> of(const std::multiset<T>&);

	template<typename R = T, std::enable_if_t<
			std::is_same<std::string, R>::value, R>* = nullptr>
	static std::unique_ptr<Stream<std::string>> of(
			const std::basic_istream<char>& stream, const Style& style);

	template<typename R = T, typename std::enable_if<
			!std::is_same<std::string, R>::value, R>::type* = nullptr>
	static std::unique_ptr<Stream<R>> of(
			const std::basic_istream<char>& stream);

	static void setLimit(std::size_t);

	bool allMatch(Predicate) const;

	bool anyMatch(Predicate) const;

	std::size_t count() const;

	Stream<T>& distinct();

	std::unique_ptr<Stream<T>> filter(Predicate);

	std::optional<T> findAny(Predicate) const;

	std::optional<T> findFirst() const;

	void forEach(Consumer);

	template<typename R>
	std::unique_ptr<Stream<R>> map(std::function<R(T)>) const;

	T max(Comparator = std::less<T>()) const;

	T min(Comparator = std::less<T>()) const;

	bool noneMatch(Predicate) const;

	Stream<T>& peek(Consumer);

	std::optional<T> reduce(BinaryOperation);

	T reduce(T, BinaryOperation);

	Stream<T>& skip(std::size_t, std::size_t start = 0);

	Stream<T>& sorted(Comparator = std::less<T>());

	std::pair<T*, std::size_t> toArray() const;

	virtual ~Stream();

};

template<typename T>
template<typename R, typename std::enable_if<
		!std::is_same<std::string, R>::value, R>::type*>
std::unique_ptr<Stream<R>> Stream<T>::of(
		const std::basic_istream<char>& stream) {
	std::clog << "other\n";

	return Stream<R>::empty();
}

template<typename T>
template<typename R, std::enable_if_t<std::is_same<std::string, R>::value, R>*>
std::unique_ptr<Stream<std::string>> Stream<T>::of(
		const std::basic_istream<char>& stream,
		const Style& style) {


	std::clog << "string\n";

	return Stream<std::string>::empty();
}

template<typename T>
std::size_t Stream<T>::limit = std::numeric_limits<std::size_t>::max();

template<typename T>
void Stream<T>::setLimit(std::size_t limit) {
	Stream<T>::limit = limit;
}

template<typename T>
Stream<T>::Stream(std::initializer_list<T> items) :
		builder_ { nullptr } {

	data.assign(items);

}

template<typename T>
Stream<T>::Stream(std::vector<T>&& data) :
		data { std::move(data) }, builder_ { nullptr } {
}

template<typename T>
Stream<T>::Stream(std::size_t size) :
		builder_ { nullptr } {

	data.resize(size);

}

template<typename T>
typename Stream<T>::Builder_t& Stream<T>::builder() {

	if (builder_ == nullptr)
		builder_ = new Builder_t(this);

	return *builder_;

}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::concat(const Stream<T>& first,
		const Stream<T>& second) {

	auto stream = std::unique_ptr<Stream<T>>(
			new Stream<T>(first.data.size() + second.data.size()));

	stream->data.assign(first.data.begin(), first.data.end());
	stream->data.insert(stream->data.begin() + first.data.size(),
			second.data.begin(), second.data.end());

	return stream;

}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::empty() {

	return std::unique_ptr<Stream<T>>(new Stream<T>(0));

}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::generate(std::function<T()> supplier) {

	if (Stream<T>::limit == std::numeric_limits<std::size_t>::max())
		throw std::runtime_error("Stream length is unspecified!!! ");

	auto stream = std::unique_ptr<Stream<T>>(new Stream<T>(Stream<T>::limit));

	std::generate_n(stream->data.begin(), Stream<T>::limit, supplier);

	return stream;
}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::iterate(T seed,
		std::function<T(T)> unaryOperator) {

	if (Stream<T>::limit == std::numeric_limits<std::size_t>::max())
		throw std::runtime_error("Stream length is unspecified!!! ");

	auto stream = std::unique_ptr<Stream<T>>(new Stream<T>(Stream<T>::limit));

	for (size_t i = 0; i < Stream<T>::limit; i++) {
		stream->data[i] = seed;
		seed = unaryOperator(seed);
	}

	return stream;

}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::of(std::initializer_list<T> items) {

	return std::unique_ptr<Stream<T>>(new Stream<T>(items));

}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::of(T item) {

	return std::unique_ptr<Stream<T>>(new Stream<T>( { item }));

}
template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::of(const std::vector<T>& vec) {

	auto stream = std::unique_ptr<Stream<T>>(new Stream<T>(vec.size()));

	stream->data.assign(vec.begin(), vec.end());

	return stream;

}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::of(const std::deque<T>& deq) {

	auto stream = std::unique_ptr<Stream<T>>(new Stream<T>(deq.size()));

	stream->data.assign(deq.begin(), deq.end());

	return stream;

}
template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::of(const std::forward_list<T>& flist) {

	auto stream = std::unique_ptr<Stream<T>>(new Stream<T>(flist.size()));

	stream->data.assign(flist.begin(), flist.end());

	return stream;

}
template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::of(const std::list<T>& list) {

	auto stream = std::unique_ptr<Stream<T>>(new Stream<T>(list.size()));

	stream->data.assign(list.begin(), list.end());

	return stream;

}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::of(const std::set<T>& set) {

	auto stream = std::unique_ptr<Stream<T>>(new Stream<T>(set.size()));

	stream->data.assign(set.begin(), set.end());

	return stream;

}

template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::of(const std::multiset<T>& mset) {

	auto stream = std::unique_ptr<Stream<T>>(new Stream<T>(mset.size()));

	stream->data.assign(mset.begin(), mset.end());

	return stream;

}

template<typename T>
void Stream<T>::forEach(Consumer consumer) {

	std::for_each(data.begin(), data.end(), consumer);

}

template<typename T>
Stream<T>& Stream<T>::peek(Consumer consumer) {

	std::for_each(data.begin(), data.end(), consumer);

	return *this;
}

template<typename T>
std::size_t Stream<T>::count() const {

	return data.size();

}

template<typename T>
Stream<T>& Stream<T>::distinct() {

	auto last = std::unique(data.begin(), data.end());

	data.erase(last, data.end());

	return *this;
}

template<typename T>
T Stream<T>::max(Comparator comparator) const {

	return *(std::max_element(data.begin(), data.end(), comparator));

}
template<typename T>
T Stream<T>::min(Comparator comparator) const {

	return *(std::min_element(data.begin(), data.end(), comparator));

}
template<typename T>
std::optional<T> Stream<T>::reduce(BinaryOperation binaryOperator) {

	std::optional<T> op;

	if (data.size() == 0u)
		return std::nullopt;

	for (T item : data)
		op.emplace(binaryOperator(op.value(), item));

	return op;
}

template<typename T>
T Stream<T>::reduce(T identity, BinaryOperation binaryOperation) {

	for (T item : data)
		identity = binaryOperator(identity, item);

	return identity;
}
template<typename T>
std::optional<T> Stream<T>::findFirst() const {

	if (data.size() == 0u)
		return std::nullopt;
	return std::optional<T>(data[0]);

}
template<typename T>
std::optional<T> Stream<T>::findAny(Predicate predicate) const {

	for (T item : data) {

		if (predicate(item))
			return std::optional<T>(item);

	}

	return std::nullopt;
}
template<typename T>
bool Stream<T>::allMatch(Predicate predicate) const {

	return std::all_of(data.begin(), data.end(), predicate);
}

template<typename T>
bool Stream<T>::anyMatch(Predicate predicate) const {

	return std::any_of(data.begin(), data.end(), predicate);

}

template<typename T>
bool Stream<T>::noneMatch(Predicate predicate) const {

	return std::none_of(data.begin(), data.end(), predicate);

}
template<typename T>
std::unique_ptr<Stream<T>> Stream<T>::filter(Predicate predicate) {

	std::vector<T> out;

	std::copy_if(data.begin(), data.end(), std::back_inserter(out), predicate);

	return std::unique_ptr<Stream<T>>(new Stream<T>(std::move(out)));

}

template<typename T>
Stream<T>& Stream<T>::sorted(Comparator comparator) {

	std::sort(data.begin(), data.end(), comparator);

	return *this;
}

template<typename T>
template<typename R>
std::unique_ptr<Stream<R>> Stream<T>::map(std::function<R(T)> mapping) const {

	auto ret = Stream<R>::empty();

	//std::transform(data.begin(), data.end(), ret->data.begin(), mapping);

	//return ret;

}
template<typename T>
std::pair<T*, std::size_t> Stream<T>::toArray() const {

	return std::make_pair(data.data(), data.size());
}

template<typename T>
Stream<T>& Stream<T>::skip(std::size_t offset, std::size_t start) {

	data.erase(data.begin() + start, data.begin() + start + offset);

	return *this;
}

template<typename T>
Stream<T>::~Stream() {

	if (builder_ != nullptr)
		delete builder_;

}
};
#endif

